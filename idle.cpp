#include "idle.h"

#include <iostream.h>

extern volatile char lockf;

IdleThread::IdleThread() : finished(0) {}

IdleThread::~IdleThread() {
	
}

void IdleThread::stop() {
	finished = 1;
}

void IdleThread::run() {
	while(!finished);
}