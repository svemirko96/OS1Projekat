#include "thread.h"
#include "lock.h"
#include "pcb.h"
#include "timer.h"
#include "SCHEDULE.h"
#include <iostream.h>

Thread::Thread(StackSize stackSize, Time timeSlice) {
	myPCB = new PCB(stackSize, timeSlice, this);
}

Thread::~Thread() {
	waitToComplete();
	delete myPCB;
}

void Thread::start() {
	myPCB->start();
}

void Thread::waitToComplete() {
	myPCB->waitToComplete();
}

void Thread::sleep(Time timeToSleep) {
	PCB::running->sleep(timeToSleep);
}

// Nephodno zakljucavanje zbog sledeceg moguceg scenarija
// 	- Nit1 pozove dispatch(), izvrsi se contextSwitchDemanded = 1, i desi se prekid od strane tajmera
//	- Nit2 dobije procesor jer je contextSwitchDemanded = 1, sto je jaci uslov za preuzimanje nego lockf = 1 (zabranjeno preuzimanje) i krece da se izvrsava
//	- Nit2 poziva dispatch(), izvrsi se promena konteksta za vreme koje se 

void dispatch() {
	asm cli;
	contextSwitchDemanded = 1;
	asm int 8h;
	asm sti;
}

// Stari dispatch
/*void dispatch() {
	lock;
	contextSwitchDemanded = 1;
	timer();
	unlock;
}*/
