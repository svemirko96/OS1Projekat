#ifndef _pcb_h_
#define _pcb_h_

#include "semaphor.h"

class Thread;

typedef unsigned long StackSize;
typedef unsigned int Time;
typedef int ID;

typedef struct StackRegs {
	unsigned sp;
	unsigned ss;
	unsigned bp;
} StackRegs;

class PCB {
public:
	// Thread states
	static const unsigned NEW;
	static const unsigned READY;
	static const unsigned RUNNING;
	static const unsigned BLOCKED;
	static const unsigned FINISHED;

	static PCB* running;
	
	static void wrapper(Thread*);
	static void setupKThread(PCB*&);
	
	PCB(StackSize, Time, void*);

	ID getId() const;
	
protected:
	virtual ~PCB();

	friend class Thread;
	friend class KernelSem;
	friend class Kernel;
	friend class SleepQueue;
	friend void interrupt timer(...);
	friend int main(int argc, char* argv[]);
	
private:
	Thread* myThread;

	// thread's call stack, it's size in bytes, and values of stack related registers
	unsigned *stack;
	StackSize stackSize; // number of 16-bit (2B) mem locations allocated to stack
	StackRegs regs;

	// given CPU time in number of time slices
	Time timeSlice;

	// thread's current state
	unsigned state;

	// thread's identification
	ID id;

	/*
	 * All the threads that call waitToComplete()
	 * method for this thread get blocked on this
	 * semaphore if this thread hasn't finished yet,
	 * and get deblocked when the thread's pcb
	 * destructor is invoked.
	 */
	Semaphore waitingThreadsSem;

	static int nextID;

	void start();
	void waitToComplete();
	void sleep(Time waitTime);
	void deblockWaitingThreads();

	void init(StackSize, Time, void*);
	void setupCallStack();
};

#endif