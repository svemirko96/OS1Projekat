#ifndef _semque_h_
#define _semque_h_

class PCB;

typedef int ID;

// Obicna FIFO lista PCB-eva
class ThreadList {
	typedef struct Elem {
		PCB* pcb;
		struct Elem* next;
		
		Elem(PCB* tpcb) : pcb(tpcb), next(0) {}
	} Elem;
	
public:
	ThreadList();
	~ThreadList();
	
	void put(PCB* pcb);		// Stavlja pcb na kraj liste
	PCB* get();				// Izbacuje iz liste i vraca prvi pcb
	PCB* getById(ID id);	// Vraca pokazivac na pcb sa datim id-em
	PCB* remove(ID id);		// Izbacuje iz liste i vraca pcb sa datim id-em
	
	unsigned size() const;
	
protected:
private:
	Elem *head, *tail;
	unsigned cnt;
};

#endif