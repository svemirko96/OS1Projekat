#include "pcb.h"
#include "lock.h"
#include "thread.h"
#include "dos.h"
#include "kernel.h"
#include "SCHEDULE.h"

#include <iostream.h>

const unsigned PCB::NEW			= 0;
const unsigned PCB::READY		= 1;
const unsigned PCB::RUNNING		= 2;
const unsigned PCB::BLOCKED		= 3;
const unsigned PCB::FINISHED	= 4;

PCB* PCB::running = 0;

int PCB::nextID = 0;

PCB::PCB(StackSize stackSize, Time timeSlice, void* myThread) :
stack(0),
waitingThreadsSem(0) {
	init(stackSize,  timeSlice, myThread);
}

PCB::~PCB() {
	if(stack) {
		delete stack;
		stack = 0;
	}
	Kernel::removeThread(id);
}

void PCB::init(StackSize _stackSize, Time _timeSlice, void* _myThread) {

	myThread = (Thread*)_myThread;

	timeSlice = _timeSlice;

	if(_stackSize > (1UL << 16))
		_stackSize = (1UL << 16);
	stackSize = _stackSize / sizeof(unsigned);
	state = PCB::NEW;

	lock;
	id = PCB::nextID++;
	unlock;

	// ako je myThread == 0 radi se o glavnoj niti koja vec ima svoj callstack
	if(myThread)
		setupCallStack();

	Kernel::addThread(this);
}

// sadrzaj steka se vestacki namesti kao da je izvrsavanje niti prekinuto interrupt-om, a sledeca operacija je PCB::wrapper(this)
void PCB::setupCallStack() {
	lock;
	stack = new unsigned[stackSize];
	unlock;

#ifndef BCC_BLOCK_IGNORE
	stack[stackSize - 1] = FP_SEG(myThread);
	stack[stackSize - 2] = FP_OFF(myThread);
#endif
	stack[stackSize - 5] = 0x0200;
#ifndef BCC_BLOCK_IGNORE
	stack[stackSize - 6] = FP_SEG(PCB::wrapper);
	stack[stackSize - 7] = FP_OFF(PCB::wrapper);

	// push ax,bx,cx,dx,es,ds,si,di,bp
	regs.ss = FP_SEG(stack + stackSize - 16);
	regs.sp = FP_OFF(stack + stackSize - 16);
#endif
	regs.bp = regs.sp;
}

void PCB::wrapper(Thread* t) {
	t->run();
	t->myPCB->state = PCB::FINISHED;
	t->myPCB->deblockWaitingThreads();
	dispatch();
}

void PCB::setupKThread(PCB*& kthread) {
	kthread = new PCB(defaultStackSize, defaultTimeSlice, 0);
	kthread->state = PCB::RUNNING;
	PCB::running = kthread;
}

void PCB::start() {
	lock;
	if(state == PCB::NEW) {
		state = PCB::READY;
		Scheduler::put(this);
	}
	unlock;
}

void PCB::waitToComplete() {
	while(state != FINISHED && state != NEW) {
		waitingThreadsSem.wait(1);
	}
}

void PCB::sleep(Time waitTime) {
	Kernel::block(waitTime);
}

ID PCB::getId() const {
	return id;
}

void PCB::deblockWaitingThreads() {
	while(waitingThreadsSem.val() < 0) {
		waitingThreadsSem.signal();
	}
}