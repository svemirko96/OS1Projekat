#ifndef _lock_h_
#define _lock_h_

//#define lock asm cli
//#define unlock asm sti

#define lock lockf = 1
#define unlock lockf = 0

extern volatile char lockf;

#endif