#ifndef _sleepq_h_
#define _sleepq_h_

class PCB;

typedef unsigned int Time;
typedef int ID;

void dispatch();

class SleepQueue {
	typedef struct Elem {
		PCB* pcb;
		Time waitTime;
		struct Elem* next;
		
		Elem(PCB* tpcb, Time time) : pcb(tpcb), waitTime(time), next(0) {}
	} Elem;
	
public:
	SleepQueue();
	~SleepQueue();
	
	void block(Time waitTime);
	//void deblock(ID id);
	void tick();
protected:
	
private:
	void put(Time waitTime);

	Elem* head;
};

#endif