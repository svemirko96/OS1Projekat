#ifndef _idle_h_
#define _idle_h_

#include "thread.h"

class IdleThread : public Thread {
public:
	IdleThread();
	~IdleThread();
	void stop();
protected:
	void run();
private:
	int finished;
};

#endif