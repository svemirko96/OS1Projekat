#include "event.h"
#include "kevent.h"
#include "lock.h"

Event::Event(IVTNo ivtNo) {
	lock;
	myImpl = new KernelEv(ivtNo);
	unlock;
}

Event::~Event() {
	delete myImpl;
}

void Event::wait() {
	myImpl->wait();
}

void Event::signal() { // can call KernelEv
	myImpl->signal();
}
