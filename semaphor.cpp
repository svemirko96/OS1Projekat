#include "semaphor.h"
#include "ksem.h"
#include "lock.h"

Semaphore::Semaphore(int init) {
	lock;
	myImpl = new KernelSem(init, this);
	unlock;
}

Semaphore::~Semaphore() {
	delete myImpl;
}

int Semaphore::wait(int toBlock) {
	return myImpl->wait(toBlock);
}

void Semaphore::signal() {
	myImpl->signal();
}

int Semaphore::val() const {
	return myImpl->val();
}
