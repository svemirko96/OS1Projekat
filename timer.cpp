#include "timer.h"
#include "pcb.h"
#include "thread.h"
#include "lock.h"
#include "SCHEDULE.H"
#include <iostream.h>
#include "kernel.h"
#include <dos.h>

extern void tick();

volatile char lockf = 0;

volatile Time tCounter = defaultTimeSlice;
volatile int contextSwitchDemanded = 0;

volatile static unsigned tsp;
volatile static unsigned tss;
volatile static unsigned tbp;

// Stari timer - definitivno nesto ne valja ovde
void interrupt timer(...) {
	
	// nije eksplicitno pozvan (dispatch), vec tajmer samo otkucava
	if(!contextSwitchDemanded) {
		if(tCounter > 0)
			tCounter--;
		Kernel::tick();
	}

	// eksplicitno pozvan ili istek dodeljenog vremena -> menjamo kontekst
	if((!lockf && tCounter == 0 && PCB::running->timeSlice != 0) || contextSwitchDemanded) {

		// --------------------- Save current --------------------
		asm {
			mov tsp, sp
			mov tss, ss
			mov tbp, bp
		}
		PCB::running->regs.sp = tsp;
		PCB::running->regs.ss = tss;
		PCB::running->regs.bp = tbp;
		// ------------------------------------------------------
		
		
		// --------------------- Choose next --------------------

		if(PCB::running == Kernel::getIdleThread()) {
			PCB* next = Scheduler::get();
			if(next) PCB::running = next;
		}
		else {
			if(PCB::running->state == PCB::RUNNING) {
				PCB::running->state = PCB::READY;
				Scheduler::put(PCB::running);
				PCB::running = Scheduler::get();
			}
			else { // BLOCKED / FINISHED
				PCB* next = Scheduler::get();
				if(next) PCB::running = next;
				else PCB::running = Kernel::getIdleThread();
			}
		}
		PCB::running->state = PCB::RUNNING;
		
		// --------------------- Restore next --------------------
		tsp = PCB::running->regs.sp;
		tss = PCB::running->regs.ss;
		tbp = PCB::running->regs.bp;
		tCounter = PCB::running->timeSlice;
		asm {
			mov sp, tsp
			mov ss, tss
			mov bp, tbp
		}
		// ----------------------------------------------------------
		
	}

	// u slucaju da poziv nije eksplicitan, odradi standardnu prekidnu rutinu
	if(!contextSwitchDemanded) {
		tick();
		asm int 60h;
	}

	// resetuj flag
	contextSwitchDemanded = 0;

}

InterruptHandler initTimer(){
#ifndef BCC_BLOCK_IGNORE
	asm cli;
	InterruptHandler oldTimerHandler = getvect(8);
	setvect(0x08, timer);
	setvect(0x60, oldTimerHandler);
	asm sti;
	return oldTimerHandler;
#endif
}

void restoreTimer(InterruptHandler oldTimerHandler){
#ifndef BCC_BLOCK_IGNORE
	asm cli;
	setvect(0x08, oldTimerHandler);
	asm sti;
#endif
}
