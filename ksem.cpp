#include "ksem.h"
#include "lock.h"
#include "pcb.h"
#include "SCHEDULE.h"

extern void dispatch();

KernelSem::KernelSem(int init, Semaphore* sem) :
value(init),
mySem(sem) {}

KernelSem::~KernelSem() {
	while(value++ < 0) {
		deblock();
	}
}

int KernelSem::wait(int toBlock) {
	int retVal = 0;
	
	lock;
	if(toBlock != 0 || value > 0) { // uslov za standardno ponasanje
		if(--value < 0) {
			block();
			retVal = 1;
		} else {
			retVal = 0;
		}
	}
	else {
		retVal = -1;
	}
	unlock;
	
	return retVal;
}

void KernelSem::signal() {
	lock;
	if(value++ < 0)
		deblock();
	unlock;
}

int KernelSem::val() const {
	return value;
}
	
void KernelSem::block() {
	lock;
	PCB::running->state = PCB::BLOCKED;
	blocked.put(PCB::running);
	unlock;
	dispatch();
}

void KernelSem::deblock() {
	lock;
	PCB* deblocked = blocked.get();
	deblocked->state = PCB::READY;
	Scheduler::put(deblocked);
	unlock;
}
