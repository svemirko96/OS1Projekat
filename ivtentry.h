#ifndef _ivtentry_h_
#define _ivtentry_h_

typedef void interrupt (*InterruptHandler)(...);
typedef unsigned char IVTNo;
class KernelEv;

class IVTEntry {
public:
	static IVTEntry* getEntry(IVTNo ivtNo);

	IVTEntry(IVTNo _ivtNo, InterruptHandler handler);
	
	void signal();

	// poziva se pri kreiranju objekta klase Event, od ovog trenutka su prekidne rutine zamenjene sve do unistenja objekta klase Event i pozivanja resetEvent()
	void setEvent(KernelEv* ev);

	void resetEvent();

	void callOldHandler();
private:
	IVTNo ivtNo;
	KernelEv* event;
	
	InterruptHandler oldHandler;
	InterruptHandler newHandler;

	static IVTEntry* entries[256];
};

#endif