#ifndef _timer_h_
#define _timer_h_

#include "event.h"

extern volatile int contextSwitchDemanded;

InterruptHandler initTimer();

void interrupt timer(...);

void restoreTimer(InterruptHandler oldTimerHandler);

#endif