#include "kevent.h"

#include "pcb.h"
#include "ivtentry.h"

KernelEv::KernelEv(IVTNo n) :
ivtNo(n),
ownerThread(PCB::running),
evSem(0) {
	IVTEntry::getEntry(n)->setEvent(this);
}

KernelEv::~KernelEv() {
	IVTEntry::getEntry(ivtNo)->resetEvent();
}

void KernelEv::wait() {
	if(PCB::running == ownerThread)
		evSem.wait(1);
}

void KernelEv::signal() {
	if(evSem.val() == -1)
		evSem.signal();
}
