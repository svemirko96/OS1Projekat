#include "ivtentry.h"
#include "kevent.h"
#include <dos.h>

IVTEntry* IVTEntry::entries[256];

IVTEntry* IVTEntry::getEntry(IVTNo ivtNo) { return entries[ivtNo]; }

IVTEntry::IVTEntry(IVTNo _ivtNo, InterruptHandler handler) :
ivtNo(_ivtNo),
event(0),
oldHandler(0),
newHandler(handler) {
	entries[ivtNo] = this;
}

void IVTEntry::signal() {
	event->signal();
}

void IVTEntry::setEvent(KernelEv* ev) {
	event = ev;
#ifndef BCC_BLOCK_IGNORE
	asm cli;
	oldHandler = getvect(ivtNo);
	setvect(ivtNo, newHandler);
	asm sti;
#endif
}

void IVTEntry::resetEvent() {
#ifndef BCC_BLOCK_IGNORE
	asm cli;
	setvect(ivtNo, oldHandler);
	asm sti;
#endif
}

void IVTEntry::callOldHandler() {
	oldHandler();
}