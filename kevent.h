#ifndef _kevent_h_
#define _kevent_h_

#include "event.h"
#include "semaphor.h"
#include "ksem.h"

class PCB;

class KernelEv {
public:
	KernelEv(IVTNo n);

	~KernelEv();

	void wait();

	void signal();

private:
	IVTNo ivtNo;
	PCB* ownerThread;
	Semaphore evSem;
};

#endif