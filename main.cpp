#include "timer.h"
#include "pcb.h"
#include "idle.h"
#include "SCHEDULE.H"
#include "kernel.h"

#include <iostream.h>

extern int userMain(int argc, char* argv[]);

typedef void interrupt (*InterruptHandler)(...);

int main(int argc, char* argv[]) {
	
	PCB* kthread = 0;
	PCB::setupKThread(kthread);

	InterruptHandler oldTimerHandler = initTimer();

	IdleThread* idleThread = new IdleThread();
	idleThread->start();
	
	int retVal = userMain(argc, argv);
	
	Scheduler::put(Kernel::getIdleThread());
	idleThread->stop();
	delete idleThread;
	
	restoreTimer(oldTimerHandler);

	if(kthread)
		delete kthread;
	
	return retVal;
}

