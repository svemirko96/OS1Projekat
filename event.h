#ifndef _event_h_
#define _event_h_

#include "ivtentry.h"

typedef void interrupt (*InterruptHandler)(...);
typedef unsigned char IVTNo;
class KernelEv;

#define PREPAREENTRY(ivtNo, callOld) \
	void interrupt interruptHandler##ivtNo(...); \
	IVTEntry ivtEntry##ivtNo(ivtNo, interruptHandler##ivtNo); \
	void interrupt interruptHandler##ivtNo(...) { \
		ivtEntry##ivtNo.signal(); \
		if(callOld) \
			IVTEntry::getEntry(ivtNo)->callOldHandler(); \
	}

class Event {
public:
	Event(IVTNo ivtNo);
	~Event();
	
	void wait();
	
protected:
	friend class KernelEv;
	void signal(); // can call KernelEv

private:
	KernelEv* myImpl;
};

#endif
