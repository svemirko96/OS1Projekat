#ifndef _ksem_h_
#define _ksem_h_

#include "thrlist.h"

class Semaphore;

class KernelSem {
public:
	KernelSem(int init, Semaphore* sem);

	virtual ~KernelSem();
	
	/*
		- toBlock != 0:
			- standardna wait operacija
			- povratna vrednost:
				- 1 - pozivajuca nit se blokirala
				- 0 - nije doslo do blokiranja niti
		- toBlock = 0:
			- value <= 0 - NEstandardno ponasanje:
				- pozivajuca nit se ne blokira
				- vrednost semafora se ne menja
				- povratna vrednost je -1
			- value > 0 - standardno ponasanje:
				- pozivajuca nit se ne blokira
				- vrednost semafora se umanjuje za 1
				- povratna vrednost je 0
	*/
	virtual int wait(int toBlock);

	virtual void signal();
	
	int val() const;

private:
	Semaphore* mySem;
	int value;
	ThreadList blocked;
	
	void block();
	void deblock();
};

#endif