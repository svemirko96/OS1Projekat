#include "sleepq.h"

#include "SCHEDULE.H"
#include "pcb.h"
#include "lock.h"

extern PCB* fullBurstThread;

SleepQueue::SleepQueue() : head(0) {}

SleepQueue::~SleepQueue() {
	Elem* p = 0;
	while(head) {
		p = head;
		head = head->next;
		// U slucaju brisanja objekta pre isteka vremena cekanja svih niti, one se deblokiraju i ranije vracaju u red spremnih
		Scheduler::put(p->pcb);
		delete p;
	}
}

void SleepQueue::block(Time waitTime) {
	if(waitTime <= 0)
		return;
	
	lock;
	PCB::running->state = PCB::BLOCKED;
	put(waitTime);	
	unlock;
	dispatch();
}

void SleepQueue::tick() {
	if(!head)
		return;
	
	head->waitTime--;
	
	// Vrati u red spremnih sve kojima je preostalo vreme za cekanje jednako 0
	while(head && head->waitTime == 0) {
		head->pcb->state = PCB::READY;
		Scheduler::put(head->pcb);
		Elem* t = head;
		head = head->next;
		delete t;
	}
}

void SleepQueue::put(Time waitTime) {
	
	lock;
	Elem* elem = new Elem(PCB::running, waitTime);
	unlock;

	// Lista je prazna
	if(!head) {
		head = elem;
		return;
	}
	
	// Nakon ove petlje curr pokazuje na prvi sledeci element koji duze ceka od ovog koji se ubacuje
	Elem *curr = head, *prev = 0;
	while(curr && (elem->waitTime >= curr->waitTime)) {
		elem->waitTime -= curr->waitTime;
		prev = curr;
		curr = curr->next;
	}
	
	// Ubacuje se na prvo mesto
	if(!prev) {
		head->waitTime -= elem->waitTime;
		elem->next = head;
		head = elem;
	}
	// Ubacuje se na poslednje mesto
	else if(!curr) {
		prev->next = elem;
	}
	// Ubacuje se negde u listi izmedju dva elementa
	else {
		prev->next = elem;
		elem->next = curr;
		curr->waitTime -= elem->waitTime;
	}
}