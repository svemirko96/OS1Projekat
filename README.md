Basic implementation of a thread subsystem for the 16-bit Microsoft environment (works on 32-bit systems as well).

The main feature of the 16-bit system used here is the direct access to the interrupt vector table (IVT), where
the timer interrupt routine was modified and used for swapping out threads if they ran out of time slices given
to execute themselves.

There is also implementation of semaphores used for thread synchronization, as well as support for events which use
previous two concepts to give some basic event driven programming support.

Project was done for a university course teaching Operating Systems.