#ifndef _kernel_h_
#define _kernel_h_

#include "sleepq.h"
#include "thrlist.h"

class PCB;

typedef unsigned int Time;
typedef int ID;

class Kernel {
public:
	static void addThread(PCB* pcb);
	static PCB* getThreadByID(ID id);
	static PCB* removeThread(ID id);
	
	static void block(Time waitTime);
	static void deblock(ID id);
	static void tick();
	
	static PCB* getIdleThread();
protected:
	
private:
	static Kernel& instance();
	Kernel();
	
	static PCB* idleThread;

	ThreadList aliveThreads;	// Evidencija o svim nezavrsenim nitima, sem idle niti
	SleepQueue sleepingThreads; // Niti blokirane pozivom Thread::sleep()
};

#endif