#include "thrlist.h"
#include "pcb.h"
#include "lock.h"

ThreadList::ThreadList() :
head(0),
tail(0),
cnt(0) {}

ThreadList::~ThreadList() {
	Elem *p = head, *q = 0;
	while(p) {
		q = p;
		p = p->next;
		delete q;
	}
}

void ThreadList::put(PCB* pcb) {
	lock;
	tail = (head ? tail->next : head) = new Elem(pcb);
	unlock;
	cnt++;
}

PCB* ThreadList::get() {
	if(!head)
		return 0;
	
	PCB* ret = head->pcb;
	Elem* tmp = head;
	head = head->next;
	delete tmp;
	
	if(!head)
		tail = 0;
	
	cnt--;
	
	return ret;
}

PCB* ThreadList::getById(ID id) {
	
	Elem* curr = head;
	for(; curr; curr = curr->next)
		if(curr->pcb->getId() == id)
			return curr->pcb;
	
	return 0;
}

PCB* ThreadList::remove(ID id) {
	
	Elem *curr = head, *prev = 0;
	for(; curr; prev = curr, curr = curr->next)
		if(curr->pcb->getId() == id) {
			PCB* pcb = curr->pcb;
			if(prev)
				prev->next = curr->next;
			else
				head = curr->next;
			delete curr;
			return pcb;
		}
	
	return 0;
}

unsigned ThreadList::size() const {
	return cnt;
}

