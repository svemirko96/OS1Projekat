#include "kernel.h"
#include "pcb.h"
#include "lock.h"

PCB* Kernel::idleThread = 0;

Kernel::Kernel() :
sleepingThreads() {}

Kernel& Kernel::instance() {
	static Kernel kernel;
	return kernel;
}

void Kernel::addThread(PCB* pcb) {
	if(pcb->getId() != 1)
		instance().aliveThreads.put(pcb);
	else
		idleThread = pcb;
}

PCB* Kernel::getThreadByID(ID id) {
	return instance().aliveThreads.getById(id);
}

PCB* Kernel::removeThread(ID id) {
	return instance().aliveThreads.remove(id);
}


void Kernel::block(Time waitTime) {
	instance().sleepingThreads.block(waitTime);
}

/*void Kernel::deblock(ID id) { // prebaciti implementaciju u SleepQueue
	lock;
	PCB* deblocked = sleepingThreads.getById(id);
	deblocked->state = PCB::READY;
	Scheduler::put(deblocked);
	unlock;
}*/

void Kernel::tick() {
	instance().sleepingThreads.tick();
}

PCB* Kernel::getIdleThread() {
	return idleThread;
}